const problem6=function (inventory)
{   if(inventory == null || inventory.length=== 0){
    return [];
    }
    let specificCar=[];
    for(let i=0;i<inventory.length;i++)
    {
        if(inventory[i].car_make == "BMW" || inventory[i].car_make == "Audi")
        {
            specificCar.push(inventory[i]);
        }
    }
    return specificCar;
}

module.exports=problem6;
