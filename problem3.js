const problem3=function (inventory)
{   if(inventory == null || inventory.length=== 0){
     return [];
    }
    let carModel=[];
    for(let i=0;i<inventory.length;i++)
    {
        carModel.push(inventory[i].car_model);
    }
    return carModel.sort();
}

module.exports=problem3;
