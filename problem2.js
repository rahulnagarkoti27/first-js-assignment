const problem2=function (inventory)
{   if(inventory == null || inventory.length=== 0)
    {
       return [];
    }

    const lastCar=(inventory.length)-1;
    return `Last car is a ${inventory[lastCar].car_make} ${inventory[lastCar].car_model}`;
}


module.exports= problem2;
