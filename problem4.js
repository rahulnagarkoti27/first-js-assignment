const problem4=function (inventory)
{   if(inventory == null || inventory.length=== 0){
     return [];
    }
    let carYears=[];
    for(let i=0;i<inventory.length;i++)
    {
        carYears.push(inventory[i].car_year);
    }
    return carYears;
}

module.exports=problem4;
