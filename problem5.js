const problem5=function (inventory)
{   if(inventory == null || inventory.length=== 0){
     return [];
    }
    let oldCar=0;
    for(let i=0;i<inventory.length;i++)
    {
        if(inventory[i].car_year<2000)
        {
            oldCar++;
        }
    }
    return oldCar;
}

module.exports=problem5;
